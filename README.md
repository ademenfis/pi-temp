## Introduction

stocazzo!!!

Web server using a Raspberry Pi and DHT22 sensor to graph the humidity and temperature in my apartment over time. The data can be accessed over a web browser.

## Summary

The main python server is `lab_app.py` and the script `env_log.py` is to be used as cronjob to take a temperature and humidity reading every so many minutes, and store it in a sqlite database `lab_app.db`. The web server has 2 views, Current at `lab_temp.html` and Historic at `lab_env_db.html`

## Current

In the current view, the current temperature and humidity taken from the sensor is displayed on the web page. This page updates every 10 seconds, and has a link to the historic view.

## Historic

The historic view has a couple features. The first is a to and from date input, which uses a date time picker plugin. Combined with a submit button, this allows you to view the temperature and humidity data within any date time range easily. The next feature are four radio buttons, to easily see the last 3, 6, 12, and 24 hours of data.

The main view are 2 tables combined with Google Charts graphs. The tables both scroll, and the charts display data across the time selected. The times are displayed in your own time zone, as it is determined from your browser.

## Circuit Diagram

![Circuit Diagram](/Circuit.png?raw=true "Circuit Diagram")

## Install

Install git + nginx + pip3

`sudo apt install -y git nginx python3-dev python3-pip`

Clone this repo

`git clone https://gitlab.com/ademenfis/pi-temp`

`cd pi-temp`
`pip3 install flask uwsgi arrow Adafruit_DHT`

Install cronjob

`crontab -e`

add `*/5 *   *   *   *    /usr/bin/python3 /home/pi/pi-temp/env_log.py`

Create log dir

`sudo mkdir /var/log/uwsgi`
`sudo chown pi:pi /var/log/uwsgi/`

Copy systemd service file

`sudo cp lab_app.service /etc/systemd/system/`

Start and enable the service

`sudo systemctl start lab_app`
`sudo systemctl enable lab_app`

Copy nginx configs and link the config to sites-enabled:

`sudo cp lab_app_nginx.conf /etc/nginx/sites-available/`
`sudo ln -s /etc/nginx/sites-available/lab_app_nginx.conf /etc/nginx/sites-enabled`

Restart nginx

`systemctl restart nginx`

Access systme at `https://<ip address>:8080/lab_temp`
